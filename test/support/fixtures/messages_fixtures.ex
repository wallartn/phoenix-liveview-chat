defmodule LiveviewChat.MessagesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `LiveviewChat.Messages` context.
  """

  @doc """
  Generate a message.
  """
  def message_fixture(attrs \\ %{}) do
    {:ok, message} =
      attrs
      |> Enum.into(%{
        message: "some message",
        name: "some name"
      })
      |> LiveviewChat.Messages.create_message()

    message
  end
end
